# Data Base Replication

- a kind of replication mechanisms in distributed systems [[replication]]
- sharing information to enable
  - availability
  - scalability
  - fault-tolerance
- data is split between mutliple instances
