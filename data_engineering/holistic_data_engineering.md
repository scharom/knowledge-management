# Holistic Data Engineering

Data projects are centered around the ETL process and its derivatives. Think
of the following three steps when creating a data project:

- Source: Identify the source system to extract form
- Ingest: Identify the technology and method to lad the data
- Prepare: Identify the technology and method to transform or prepare the data

Also consider the applications that will finally consume the data of the ETL process:

- Analyze: Identify the technology nad method to analyze the data
(e.g.: the tech that the data scientist will use)
- Consume: Identify the technology and method to consume and present the data
(e.g.: BI-Stuff)

The presented project phases are not to be followed linearly.