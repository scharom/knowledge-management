# Operational Needs

- How will the data be used?
- What are the requirements?
- What is the best data storage solution respecting the requirements?

## Operations and latency

- What are the main operations performed on the data provided by our platform?
- 