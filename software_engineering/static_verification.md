# Static Verification

- goal is to find errors early
- implemented software is not executed
- can be done before and after implementation
- manual or automated review of documents that describe the software that should be implemented
- should catch a defect early in the software testing life cycle

# Before implementation

- Requirements specification
- Design Document
- Test Plans
- Test Cases

## After implementation

- Compiling
- Proofing