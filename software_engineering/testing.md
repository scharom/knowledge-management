# Dynamic Testing Techniques

- is conducted in the framework of a [[test_plan]]
- should increase confidence that written code (the [[system_under_test]]) does what's expected
- code bases evolve over time, tests prevent a change in the behavior (regression)
- builds the [[dynamic_verification]]

## Types of testing

### Functional testing

- [[unit_test]]
  - [[example_based_testing]]
  - [[property_based_testing]]
- [[mocking]]
- [[integration_test]]
- [[system_test]]

### Non-functional testing

- [[security_testing]]

## Quality of testing

- things to ponder: tests are code too -> can have bugs themself

### Test Coverage

- can tell the code paths that are not tested
- does not tell anything about the coverage of the input domain

### Test-Driven Development

- key idea: test before implementing a bug fix
- write a test
- let the test fail -> bug is present
- wirte an implementation