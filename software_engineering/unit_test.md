# Unit Test

- programm that calls the program under test with parameters where the expected result is known
- tests are placed in a test suite (often mirrors the name of the source files)
- several different testing libraries
- can use be based on
  - [[example_based_testing]] 
  - [[property_based_testing]]

## Steps of unit testing

1. test case generation (test case generation problem)
2. program evaluation
3. output analysis (oracle problem)


## Libraries

- [[munit]]