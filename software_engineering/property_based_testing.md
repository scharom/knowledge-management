# Property Based Testing

- input
  - generates random input data
  - increases the input domain of the [[system_under_test]]
  - ranges for the input values can be explicitly given via generators
- output
  - expected output is not clear (bad at [[oracle_problem]])
  - the output is not checked on correctness
  - the output must be valid one some defined properties
- example libraries:
  - [[scala_check]]

## Traps

- rewriting [[system_under_test]] after a failed PBT is likely to reproduce the same bug
- forcing an oracle (rewriting the [[system_under_test]] in a more complicated way)

## Strategies for identifying properties

- test against an oracle (correct implementation)
- validity: check if output is valid
  - with sets of stings
  - provides a sanity check
- invertibility
  - aimes for persistence
- idempotence on a function
- invariances
  - example: difference between sorted list and unsorted list is zero
- metamorphic relation
  - intuition: a couple of transformation such that the applying one befor the SUT and the other doesn't change the outcome
- shit ton of literature how to find good properties

## Generators

- kind of oracle and specification for the system