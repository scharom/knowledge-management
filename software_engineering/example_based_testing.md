# Example Based Testing

- provide one expected outcome for one input at a time
- good for regression tests
- good at the [[oracle_problem]]
- it is bad because of bad test case generation
  - missing specifications
  - author's bias: author have tested everything that author has implemented (you can not prove to your self that you have thought of everything)
  - insufficient tests: not the whole domain is covered