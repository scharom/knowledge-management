# Integration Test

- when components of a system are assembled together new problems do arise
- the practice of testing the whole system (no stubs or mocks)
- challenge: you need to setup the whole infra programatically to a specific state
- makes the system closer to what is deployed into production

# Libraries

- [[munit]]