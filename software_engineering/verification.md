# Verification

- Check [[implementation]] against the [[specification]]
- Have we built our product right?
- two kinds of verification
  - [[static_verification]]
  - [[dynamic_verification]]
