# System Test

- evaluation of system from end-to-end
- should exercise the full computer-based system with
- is a kind of [[black_box_testing]]
- tests the fully integrated application with all peripherals
- tests the user experience of the application
