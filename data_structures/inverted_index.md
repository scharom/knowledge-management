# Inverted Index

- a data structure that represents an [[index]]
- back in the day: term document matrix
  - every document and term combination has a weight
  - the matrix is sparse (storage is wasted)
- Inverted index is a multi map
- components
  - vocabulary file (maps terms to start of posting list)
  - postings files(s)
  - [[posting]] (reference to the document and weight)
- design questions for posting
  - order of the postings in post list (depends on the queries of the users)
  - which information should be saved in a posting