# Schleifenfreier Graph

Schleifenfrei (oder *loop-free*) sind Graphen ([[graph]]), in denen ein Knoten $x$ nicht direkt mit sich selbst verbunden ist $\forall x \in V: \{x, x\} \not\in E$ .