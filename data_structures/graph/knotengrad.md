# Knotengrad

Der *Knotengrad* $\deg(x)$ ist die Anzahl der Nachbarn eines Knotens = $\left| \{y \in V \mid \{x, y\} \in E\} \right|$ =  Anzahl inzidenter Kanten.
$e$ ist inzident zu $x \Leftrightarrow x \in e$ .

Für gerichtete [[graph]]n:
- $\deg_\text{in}(x) = |\{y | (y, x) \in E\}|$
- $\deg_\text{out}(x) = |\{y \mid (x, y) \in E\}|$