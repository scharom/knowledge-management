# Offene und Geschlossene Kantenzüge

[[pfad]], [[weg]] und [[kantenzug]] sind *offen*, wenn $x_1 \not = x_n$ ($x_n$ ist der letzte Knoten im Kantenzug) und geschlossen, wenn $x_1 = x_n$.