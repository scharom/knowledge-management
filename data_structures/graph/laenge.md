# Länge

Die *Länge* eines [[kantenzug]] ist die Anzahl der Kanten. Ein einzelner Knoten $(x_1)$ ist ein Kantenzug der Länge $0$.

$\rightarrow$ Die Länge von Wegen ist durch $|E|$ beschränkt. ($|M|$ ist die Kardinalität einer Menge $M$ -- die Anzahl ihrer Elemente)
