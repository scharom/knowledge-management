# Adjazenz Matrix

Eine Adjazenz-Matrix $A$ ist eine Möglichkeit, einen [[graph]] numerisch zu beschreiben.
Die Elemente werden dabei folgendermaßen berechnet:
![](../attachments/adjazenz_matrix_2021-10-18-12-55-49.png)

Die Spalten- und Zeilen-Indizes sind also die Knoten-Indizes.
Ist ein Knoten mit einem anderen verbunden, ist das korrespondierende Element in der Matrix $1$, ansonsten $0$.
Die Elemente der Hauptdiagonalen der Adjazenz-Matrix sind $0$ für einen schleifenfreien Graph.
Ist der Graph ungerichtet, so ist die Matrix symmetrisch (spiegelsymmetrisch bezüglich der Hauptdiagonalen).
