# Graph

- Ein Graph ist ein Tuple aus 2 Mengen $G = (V,E)$
  - Knotenmenge $x,y,z \in V$
  - Kantenmenge $e, f, g \in E \subset V^2$ mit $V^2 = \left\{ \{x, y\} \mid x, y \in V, x \not = y \right\}$.
    In *ungerichteten* Graphen gilt für eine Kante $f =\{x, y\} \equiv \{y, x\}$.
    ($\equiv$ ist die Equivalenzrelation)

![](../attachments/graph_2021-10-18-11-50-47.png)