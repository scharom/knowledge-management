# Zusammenhängender Graph

Ein [[graph]] $G = (V, E)$ ist *zusammenhängend*, wenn je zwei Knoten $x, y$ durch einen Kantenzug verbunden sind. 
