# Eulergraph

Notwendige Bedingungen für Eulerschen [[graph]]:
- $G$ ist zusammenhängend
- Jeder Knoten in $G$ hat geraden Grad
- Enthält einen [[eulerscher_kreis]]

## Zerlegung von Eulergraphen

Ein “innerer” Kreis in E ist C
![](../attachments/eulergraph_2021-10-18-12-49-21.png)

In $G\setminus C$ hat jeder Knoten wieder geraden Grad:
- $\deg_{G\setminus C}(x) = \deg_G(x)$ wenn $x \not \in C$ 
- $\deg_{G\setminus C}(x) = \deg_G(x) -2$ wenn $x$ auf $C$ liegt

Jeder Eulerkreis kann in eine Menge von *Kanten-disjunkte*, elementare Kreisen zerlegt werden.

