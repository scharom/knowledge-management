# Eulerscher Kreis

Ein geschlossener [[weg]] der genau alle Kanten enthält heißt *Eulerscher [[kreis]]*. Enthält ein Graph einen Eulerschen Kreis, nennt man ihn [[eulergraph]].
