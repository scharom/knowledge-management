# Kantenzug

Ein Kantenzug in einem [[graph]] ist eine Sequenz von über Kanten zusammenhängende Knoten: $(x_1, e_1, x_2, e_2, x_3, e_3, x_4)$. Zwei aufeinanderfolgende Kanten enthalte immer mindestens einen gemeinsamen Knoten $(\dots e', x, e" \dots)\quad x \in e' \cap e"$.
![](../attachments/kantenzug_2021-10-18-12-06-03.png)

## Lemma
Jeder Kantenzug von x nach p enthält einen Weg von x nach p.
Jeder Kantenzug von x nach p enthält einen Pfad von x nach p.