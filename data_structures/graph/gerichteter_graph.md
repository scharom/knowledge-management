# Gerichteter Graph

Den Kanten im [[graph]] wird eine Ordnung gegeben. Dadurch wird aus der Menge $\{x, y\}$ die eine Kante beschreibt, ein Tupel $(x, y)$. $x$ wird in dem Fall als *Tail* (Schwanz) und $y$ als *Head* (Kopf) bezeichnet.
        Die Relation geht dabei von Head in Richtung Tail aus.
        Die für ungerichtete Graphen beschriebene Equivalenzrelation gilt nicht mehr $(x, y) \not\equiv (y, x)$.
