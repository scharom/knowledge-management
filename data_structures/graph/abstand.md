# Abstand

Wenn $G$ zusammenhängend ist, dann sei der *Abstand* $d(x, y)$ von zwei Knoten $x, y$ die Länge des *kürzesten* [[pfad]] zwischen $x$ und $y$.

Eigenschaften einer Metrik:

- Identität $d(x,x) = 0$
- Symmetrie $d(x,y) = d(y,x)$
- Dreiecksgleichung $d(x,y) \leq d(x,z) + d(z,y)$