# Graph Isomorphie

Ein Isomorphismus von $G \to G'$ ist eine bijektive (also umkehrbare) Abbildung $\varphi: V(G) \to V(G')$.
Für die Kanten:
$\{x, y\} \in E(G) \Leftrightarrow \{\varphi(x), \varphi(y)\} \in E(G')$
Verbal (aus sicht der Knoten):
Graphen sehen gleich aus, haben aber eine unterschiedliche Permutation von labels auf den Knoten.
Somit ist die [[adjazenz_matrix]] gleich aussehender [[graph]] unterschiedliche.
Generell ist das Finden von Isomorphien sehr schwer.

Einige notwendige Bedingungen:
- $|V_1| = |V_2|$ -- Kardinalitäten der Knotenmengen sind gleich
- $|E_1| = |E_2|$ -- Kardinalitäten der Kantenmengen sind gleich
- Gradsequenzen der Graphen sind gleich
        $\forall x \in V: \deg_G(x) = \deg_{G'}(\varphi(x))$
        Eine Gradsequenz ist dabei die Sequenz der Anzahl von Knoten pro Knotengrad, beginnend bei $0$.
        Ein Knotengrad von $0$ bedeutet, dass der Knoten keinen Nachbarn hat.
        Besteht ein Graph also zum Beispiel aus $2$ isolierten Knoten, so ist seine Gradsequenz $2$.
        Ein Graph mit einem isolierten Knoten und $2$ miteinander verbundenen Knoten hat eine Gradsequenz von $1 2$.


