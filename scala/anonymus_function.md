# Anonymous Function

- A [[function]] that is defined inline
- The parameters are left to `=>`
- Also known as lambdas
- Types of parameters can be omitted if they are clear to the compiler from the
context
- Example

``` scala
(x: Int) => x + 1
```
