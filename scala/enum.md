---
tags: domain-modeling
---
# Enum

- Creates a [[type]]s that consists of a finite set of named values
- `Enums` contain a set of data constructors
- Can be used for [[pattern_matching]]
- Enums can be parameterized
- Models a [[sum_atd]]

    ```
    enum Color(val rgb: Int):
        case Red   extends Color(0xFF0000)
        case Green extends Color(0x00FF00)
        case Blue  extends Color(0x0000FF)
    println(Color.Green.rgb) // prints 65280
    ```

  - Can have fields and methods
