# munit

- a testing framework in scala supported by metals
- supports
  - [[scala_check]]
  - [[unit_test]]
  - [[integration_test]]

## Integration test

- two approaches
  - tear dependencies for each test up and down within each of the tests
  - tear the dependencies up before all tests and down after all tests