---
tags: 
---
# Collection

- A collection in scala can hold other data
- Most common actions on collections:
  - construction
  - query data
  - transformation
- Most [[immutable]] collections are available by default
- Mutable collections must be imported `import scala.collection.mutable`
- Basic collections [[list]], [[Map]] and [[array_buffer]]

## Construction

- `empty` method on the [[companion_object]] to construct an empty collection
- varargs constructor
- `+:` and `:+` to prepend and append elements to sequences
- `+`  to add an element to a map
- `++` to append collections
- `+=:`.`+=`, `++=`, `-=` and `--=` for mutable operations on sequences
