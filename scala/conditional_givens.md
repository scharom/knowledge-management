# Conditional Givens

- a given instance that relies on another [[contex_parameter]]
- example

``` scala
given orderingList[A](using ord: Ordering[A]): Ordering[List[A]] with ...
```

- an abitrary number of conditional definitions can be combined until the search hits a "terminal" given definition

    ``` scala
    given a: A= ... // this is a terminal
    given aToB(using a: A): B = ...
    given bToC(using b: B): C = ...
    given cToD(using c: C): D = ...

    summon[D] // is translated by the compiler to summon[D](using cToD(using bToC(using aToB(using a))))
    ```
