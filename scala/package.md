# Package

- allow to give a prefix to the definitions
- avoids naming collisions
- source files structure should mirror the package structure
- Avoid full qualified referencing by using imports
- is an important concept to enable [[modularity]]
