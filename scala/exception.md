# Exception

- an exception indicates that there is some undesired state in the program.
- can have several reasons
  - programmer error
  - user error
  - hardware error
