---
tags: 
---
# Evaluation

- A program in scala is evaluated by using a substitution model
- Expressions are reduced to a [[value]]
- Formally known as $\lambda$-Calculus
- Evaluation is determined by the evaluation strategy
  - [[call_by_value]]
  - [[call_by_name]]