# Pattern Guard

- An if statement in the case of [[pattern_matching]]
- Example

``` scala
def dropWhile[A](as: List[A], f: A => Boolean): List[A] =
  as match
    case Cons(hd, tl) if f(hd) => dropWhile(tl, f)
    case _ => as
```