# Protected

- A [[visibility]]
- Protected members are visible form the inside of a trait or class and from the inside of its descendants
- Related to object oriented programming and more specifically about inheritance
