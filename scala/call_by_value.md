---
tags: 
---
# Call By Value

- Is the default [[evaluation]] strategy
- Call by name is ortogonal to [[call_by_value]]
- This distinction is made by [[parameter]]s of [[function]]s, [[method]]s or [[constructor]]