# Range

- Is an ordered [[sequence]] of integers that are equally apart
- Constant space: only start, end and current value are saved
- `1 to 3` incluedes 3
- `1 until 3` excludes 3