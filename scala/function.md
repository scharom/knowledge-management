---
tags: 
---

# Function

- A function can be used as values
- A function has a [[signature]] and [[definition]]
- Functions have a type like A => B
- Functions can be defined by the arrow syntax : `(x: Int) => x + 1`
- Functions are objects
- Calling a function means calling its apply method
- methods come from the object oriented world and are always associated to an object
- When only one argument is used consider a wilde card argument `val increment: Int => Int = _ + 1`