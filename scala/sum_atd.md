---
tags: 
---
# Sum ADT

- Is a kind of [[algebraic_data_type]]s
- Data with different finite alternatives
- Can be implemented as [[enum]]s