# Opaque Type

  ``` scala
    object UserID:
        opaque type UserID = Long
    end UserID
  ```

- inside the scope the alias definition is transparent
- outside the scope the alias is "opaque": it hides the type it is an alias to
- the object that defines an opaque type should define methods that produce and consume opaque types
- opaque types can have [[extension_methods]]