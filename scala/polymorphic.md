# Polymorphic

- A [[function]] is polymorphic when its inputs do not rely on one type
- Concrete types are abstracted from [[monomorphic]] functions that share a 
similar structure to a generic type
- This is [[parametric_polymorphism]]

