# Lazy list

- A [[list]] whose elements are only computed when really needed [[lazy_val]]
- Allows to model infinite collections