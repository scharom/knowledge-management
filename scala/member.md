---
tags: 
---
# Member

- A member of a [[class]]es is either
  - a method
  - a variable (in this context sometimes called field)
- To avoid that members are overridden prefix their declaration with `final`