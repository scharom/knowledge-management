---
tags: 
---

# Constructor

- constructor [[parameter]]s of simple [[class]]es are [[private]]
- constructor [[parameter]]s of [[case_class]]es are [[public]]
- case classes do an aggregation
- calling the constructor creates an instance at runtime