---
tags: domain-modeling
---

# Algebraic Data Type

- An algebraic data type is the composition of multiple [[type]]s into a new one
- Thus it can be used to describe how to structure data (often used in [[fp_modeling]])
- ADTs
  - Summation ADT: A type can be either of the defined subtypes
  - Product ADT: Modeled by case classes
    - Case classes are named tuples
    - Cartesian product of the input params forms the named tuples
  - Hybrid ADT: combination of Summation ADT and product ADT
- Example:
```
enum Color(val rgb: Int):
  case Red   extends Color(0xFF0000)
  case Green extends Color(0x00FF00)
  case Blue  extends Color(0x0000FF)
  case Mix(mix: Int) extends Color(mix)
```

- Advantages
  - highly composable
  - just data not functionality => leads to [[immutable]] data structures
- Side note: with immutable data structures it is easy to track wich value come form where in the code base
- In Scala 3 ADTs are often modeled via [[enum]]s instead of cases classes and sealed traits