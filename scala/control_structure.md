---
tags: 
---
# Control Structure

- scala has the basic control structures you’d expect to find in a programming language, including:
  - if/then/else
  - for loops
  - try/catch/finally
- It also has a few unique constructs, including:
  - [[match_expression]]s
  - [[for_expression]]s
