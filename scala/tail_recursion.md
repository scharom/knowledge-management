# Tail Recursion

- Tail Position: The recursive call must be the last statement in the function
- The size of the call stack can overflow with regular [[recursion]]
  - Every time the function is called the arguments are stored on the call stack
- Tail recursion can be optimized by the compiler (tail call elimination)
  - Translation of recursion into a loop
