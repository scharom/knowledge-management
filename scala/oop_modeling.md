---
tags: domain-modeling
---

# OOP Modeling

## Traits

- Abstract members of traits are services that are required by the subclass
- Concrete members of traits are services that are provided to the subclass
- Can be combined to Mixins
  
## Classes

- Should be the leafs of the inheritance model
- classes can be only extended by classes defined in the same file
  - this can be disabled with the `open` keyword
  - this enforces library creators to explicitly think about future extensability
