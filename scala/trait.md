---
tags: domain-modeling
---
# Trait

- Can contain:
  - Abstract members:
    - abstract methods `def m(): T`
    - abstract value definitions `val x: T`
    - abstract type members (type T), potentially with bounds `type T <: S`
    - abstract givens `given t: T`
  - Concrete methods and fields
- [[class]]es can extend a trait via the `extends` keyword
- Most common use of traits it as an interface
  - Trait defines the abstract members that will be implemented by a class that extends the trait
  - Traits define functionality that sould be provided by a class

- Multiple traits can be combined to Mixins [[multi_inheritance]]
- A trait can be sealed
- Every extending subclass must be defined in the same file as the sealed trait