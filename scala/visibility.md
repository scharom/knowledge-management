# Visibility

- The concept of information hiding
  - Treating an object in some respects as a “black box” and ignoring the details of its implementation
  - What [[member]]s are exposed to users of an object or a class
- Handled via access modifiers [[public]] [[private]] [[protected]]