# Substitution Model

- Similar to an algebraic equation and equational reasoning:
  - expand every part of the expression
  - replace all variables with the referents
  - reduce to the simplest form