# Type Directed Programming

- compiler can provide a value to an indicated [[type]]
- reduces boilerplate code
- compiler can only provide a value if it is "obvious"
- in scala type directed Programming is achieved with [[contex_parameter]] by two steps
  - indicating the place where the compiler should automatically insert a value 
  - provide candidate values for the compiler and the arguments