# Flatten

- Method on nested [[collection]]s
- Removes one nesting level
- Example `List(List(1,2,3),List(1,2,3)).flatten` equals `List(1,2,3,1,2,3)`