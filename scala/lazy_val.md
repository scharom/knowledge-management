---
tags: 
---
# Lazy val

The keyword lazy allows a [[value]] to not be evaluated [[evaluation]] until the very first time it is called.
If you call the value again, the previously computed value will be used.
In General:
- val : Computed once when declared
- lazy val : Computed once when called
- def : Computed each time it is called
