---
tags: domain-modeling
---
# Case Class

- Is a special kind of [[class]] 
- The biggest advantage of case classes it that they support [[pattern_matching]]
- In domain modeling case classes are used to aggregate several concepts together
- Case Classes are [[immutable]] data [[type]]s
  - Alternation of members via copy method
  - Avoids race conditions in concurrent programs
- Case classes provide several methods for free
  - apply
  - unapply (kind of extractor)
  - equals (equality on the **values** not on the sole reference)
  - copy
  - toString
- Unapply is used to deconstruct a case class to perform a [[pattern_matching]]
- Case classes are used for the [[product_adt]]
- Instantiation does not need the `new` keyword
- Default visibility of [[member]]s: [[public]]