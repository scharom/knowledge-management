---
tags: domain-modeling
---

# Domain Modeling

- Incorporates the behavior and data of the real world into a scala program
- Tools to model the real world
  - Used for [[fp_modeling]]
  - Used for [[oop_modeling]]
- Interplay between FP and OOP reaps best of both worlds
- Difficult task:
  - Finding the right level of abstraction
  - How much FP vs. OOP?
- No systematic methodology to domain modeling
- Some hints:
  - Identify the concepts
  - Identify the relations between concepts
    - One concepts contain others (Part of hierarchy)
    - Generalization (class hierarchy / sealed traits)
- After a model is sketched
  - Check that you can construct meaningful values from the model
  - Check that you can **not** construct nonsensical values from the model
- Iterate!