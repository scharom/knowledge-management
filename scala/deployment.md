# Deployment

- can take various forms
  - publish an artifact
  - package program and dependencies into a jar
  - publish a docker image
