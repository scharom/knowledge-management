---
tags: domain-modeling
---

# Sealed Class

- [[trait]]s and [[class]]es can be `sealed`
- Then all subtypes must be declared in the same field
- Assures that all subtypes are known