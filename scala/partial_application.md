# Partial Application

- A [[function]] that is applied to some but not all parameters it requires
- The not applied parameters are saved in the scope of the outer function and can be accessed when the inner function is finally invoked
- Also called Closure
- Example

``` scala
def partial1[A, B, C](a: A, f: (A, B) => C): B => C
```
