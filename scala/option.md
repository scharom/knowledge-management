---
tags: 
---
# Option

- Kind of [[collection]] that has zero or one element
- May or may not hold a value
  - Some
  - None
- Variables name or fields names that reference an Option are prefixed with *maybe* by convention
- Option collection indicates that the value might not be present
- Compiler will ensures to handle both cases
- When an Option is mapped it returns an Option agian
- In other languages use [[null]] if a value is not available
  -> should be avoided in Scala
