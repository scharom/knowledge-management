---
tags: 
---
# Pattern matching

- Pattern matching with a is often used in the `match ... case...` [[match_expression]]s
- Pattern matching checks a [[value]], also called target against a pattern
- Often [[case_class]]es are pattern matched
- A matching value can be deconstructed
- Deconstruction can happen in a recursive manner, when one cases class consists of another one
- If multiple cases match the target, the first matching case is taken