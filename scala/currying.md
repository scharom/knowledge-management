# Currying

- Converts a [[function]] that takes multiple [[argument]]s into a sequence of functions that take each argument at a time
- Uses [[partial_application]]