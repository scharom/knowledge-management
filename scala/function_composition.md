# Function Composition

- A [[function]] that takes multiple functions
- Applies one function to another function
- Scala does also provide `.compose` on functions