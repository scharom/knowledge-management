# Transformation

- A transformation is applied to a [[collection]]
- Transformations:
  - [[map_function]]
  - [[flat_map]]
  - [[fold_left]]