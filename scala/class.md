---
tags: domain-modeling
---

# Class

- A class creates a new [[type]]s
- Classes are templates for object instances
- Classe contain data and behavior
- Classes are instantiated by a [[constructor]]
- Constructros can be called with `new` (in scala3 this is optional)
- Can inherite from multiple [[trait]]s but only from one parent class

## Fields and methods

- data
  - Definition of a parameter in the constructor does automatilcally construct a field in the class
  - Costructor parameters can be immutable (`val`) or mutable (`var`)
- behavior
  - methods in the class body
- The constructor spans the whole body of the class definition
  - does also execute all method calls and all variable assignments in the body
- Constructor params can be initialised with default values
`class Socket(val timeout: Int = 5_000, val linger: Int = 5_000)`