---
tags: domain-modeling
---

# Companion Object

- Is a Singleton
- Contains functions for creating and working with the corresponding type
- [[object]]s that have the same name as a [[class]]es
- Must be declared in the same file as a class
- Useful for [[method]]s and [[value]]s that are not specific to one instance of the companion object's class
- Group static methods under a common namespace
- Can hold memory intensive data structures that must be accesses by all instances of the companion object's class