# Purity

- a [[function]] is pure when
  - it has no side effects
  - its arguments are referentially transparent [[referential_transparency]]