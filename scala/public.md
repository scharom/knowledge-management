# Public

- A [[visibility]]
- Public keyword does not exist, this is the default behavior of [[member]]s
- public members are visible from the outside of a class or trait