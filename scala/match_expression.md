---
tags: 
---
# Match expression

- A value is matched against different cases by using [[pattern_matching]]
- Similar to `switch` in other languages
- Supports wilde card parameter `case A(b,c,_) => ???` (certain values can be ignored)
- **Pattern guards** `case A(c) if c > 9 => ???`
- **Type only matching** `case a:A => ???`
  - It is a convention to use the first letter of the type as the case identifier (a, A)
- In scala three multiple match blocks can follow each other
- Match expressions can be used as the body of a method or [[function]]s
- Catch all case is provides by `case _ => ???`
- When all possible matchable types or subtypes are known the default case is not necessary
    (e.g when using [[sealed_class]]es)