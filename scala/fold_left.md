# FoldLeft

- [[transformation]] that allows to return another [[type]]
- General form:

``` scala
F[A].foldLeft(B)((B,A)=> B)==B
```

- Takes two parameters
  - A starting value of the accumulator
  - A function to combine an accumulater with the each value of the elements of the collection
- No containers involved -> more general than map and flatMap
- If a container type is used as input another container type can be returned