---
tags: 
---
# List

- Lists are immutable sequences
- Sequences are [[collection]]s
- Can be constructed and decomposed with "::" operator
- Not optimized for random access
- Accessing head and tail is efficient