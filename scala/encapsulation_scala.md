# Encapsulation

- Example: hiding a database connection
- For differing behaviors of implementations that are hiding details use [[trait]]
- Is an important concept of [[domain_modeling]]
- Can create abstraction barriers