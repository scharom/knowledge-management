# Type Variable

- A [[variable]] on a [[type]]
- Is used in [[polymorphic]] [[function]]s
- A function has [[type_parameters]] which can be referenced in the function signature