---
tags: 
---
# Signature

- The signature of a [[function]] contains:
  - a specifier name
  - an optional parameter list
  - an optional return type
- The signature is on the left hand side of the =
