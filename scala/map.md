# Map

- A [[collection]] that map keys to values
- Construction via [[tuple]]s
- Constant lookup time
- has different implementations (time guarantees and ordering of keys differ)P