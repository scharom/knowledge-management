---
tags: 
---

# For Expression

- In OOP for loops are used for side effects
- For expressions are used for the creation of a new [[collection]]s from an old one
- Example:
  
   ```scala
    val nums = Seq(1,2,3)
    val doubledNums = for (n <- nums) yield n * 2
    ```

- General syntax  `for (s) yield e`
- s is a sequence of [[generator]]s and [[filter]]s
- `yield` indicates that a new collection should be generates using the elements of the old one
- Compiler translates a for expressions to
  - map
  - flatMap (when there are multiple generators)
  - withFilter(lazy variant)
