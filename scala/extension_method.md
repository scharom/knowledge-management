# Extension Method

- allows to add a method to a type after the type is defined
- can be used to provide a nicer syntax for the method(s) of a [[type_class]]
- extensions methods of type Type T if: 
  - they are visible
  - the are defined in a companion objection associatied with T
  - they are defined in a given instance associated with the type T
- example

  ``` scala
  case class Circle(x: Double, y: Double, radius: Double)

  extension (c: Circle)
      def circumference: Double = c.radius * math.Pi * 2

    // now you can call circumference on a Circle
    val circle = Circle(0,0,1)
    circle.circumference
  ```
