---
tags: 
---
# Call By Name

- Call by name is ortogonal to [[call_by_value]]
- This distinction is made by [[parameter]]s of [[function]]s, [[method]]s or [[constructor]]
- Only the references are passed around [[evaluation]] will only happen the [[value]] in the [[parameter]] is really needed
- Enabled by `: =>`

## Example

```scala
def myIf(predicate: Boolean, ifTrue: => Int, ifFalse: => Int): Int = {
  println("calling myIf")
  if(predicate) ifTrue else ifFalse
}
```