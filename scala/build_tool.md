# Build Tool

- automates the following steps
  - compilation [[compiler]]
    - first fetches [[dependency]]
  - running [[run_time]]
  - testing [[test_scala]]
  - [[deployment]]
- [[sbt]] is a commonly used build tool for scala
  - others are maven, gradel, mill