---
tags: 
---
# Immutable

- Object is not changeable
- State can not be altered after the creation
- Inherently thread-safe
- [[variable]]s defined by `val` keyword are immutable
- Two instances of mutable objects are not the same, they have distinct identities