# Referential Transparency

An [[expression]] is referentially transparent if the expression can be exchanged with its evaluated value without changing the behavior of the program.