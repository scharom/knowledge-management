---
tags: domain-modeling
---
# Abstract Class

- [[class]]es that have non implemented methods
- Used instead of [[trait]]s when:
  - The base class should take [[constructor]] [[argument]] (also possible for traits in scala 3)
  - The code will be called from java code
- Delays the implementation of operations to concrete classes
- A class can only extend one other class but multiple traits, thus traits provide more flexibility