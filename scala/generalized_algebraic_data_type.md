---
tags: 
---
# Generalized Algebraic Data Type

- A more general type of [[algebraic_data_type]]s
- Enums can take [[parameter]]s
- Use parameter to **abstract away** the type of the parameter
- Type can be regained with [[pattern_matching]]
- Example
    ```
    enum Box[T](contents: T):
    case IntBox(n: Int) extends Box[Int](n)
    case BoolBox(b: Boolean) extends Box[Boolean](b)

    def extract[T](b: Box[T]): T = b match
    case IntBox(n)  => n + 1
    case BoolBox(b) => !b
    ```