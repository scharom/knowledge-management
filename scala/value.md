---
tags: 
---

# Value

- a represntation of data in memory at the runtime
- can be manipulated by the program
- members of [[type]]s are values
- values can be assigned to [[variable]]s