# Variadic Function

- A [[function]] that takes zero or more arguments
- Example

``` scala
def apply[A](as: A*): List[A] =
  if as.isEmpty then Nil
  else Cons(as.head, apply(as.tail*))
```