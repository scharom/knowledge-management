# Type Class

- a class that classifies the types by the operations they support
- provide retro active extension: the ability to extend a data type with new operations without changing the original definition of the data type
- Type classes are preferred over subtyping  
