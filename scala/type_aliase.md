# Type Aliase

- type aliases like `type UserId = Long` are possible
- type aliases have no runtime cost when primitive types must be wrapped
- can be handy for complex type expression
- example: `type AgeAndUsers = (Int, Seq[User])`
- use [[opaque_type]]s instead of type aliases to make the type and the aliase distinct