# Monomorphic

- A [[function]] that acts on one type of data
- The parameters of a function are fixed to one type
- The opposite of [[polymorphic]]