# Filter

- Is a [[transformation]] on a collection
- Other filters:
  - filterNot
  - withFilter (lazy filter)
