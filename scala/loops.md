# Loops

- There are several ways to perform a loop
    - iteration on standard collection
    - imperative loops with while [[control_structure]]
    - functional loops with [[recursion]]