# Error Handling

## Sources of Errors

- wrong Input (user issue)
- buggy Program (programmer issue)
- hardware failure

## Solutions

- abort program
- Inform the user
- try to recover -> robustness

## What happens when there is a problem

- when something unexpected happens an [[exception]] is thrown (with keyword `throw`)
- exceptions can be caught by exception handler
- in scala there is no checked exception
  - exceptions do not have to be noted in the method head
  - any method can throw any exception

## General notes on error handling

- exceptions have a [[type]] hierarchy
- fatal exceptions: Exceptions that can not be reacted to.
- exceptions can be pattern matched [[pattern_matching]]
- error handling is part of [[defensive_programming]]

## Explicit error handling

### Explicit error handling of exceptions with Try

- exceptions and `Try` will stop the program execution on the first encountered error
- in scala it is good practice to explicitly model exceptions
  - inform caller of a method that failure cases must be handled by using the `Try` type
  - `Try` wraps the code with the section that is possibly throwing an exception
  - the caller handles the thrown exception by calling `recover` on the return value
- use the `Using` object instead of `Try` when dealing with resources 
  - resources will always be closed
- use map and FlatMap on the resulting Try to postpone error handling  

## Explicit error handling of data validation

- in this case we like to provide a list of errors to the program's user
  -> this is not possible with exceptions and Try
- the recovery of validation errors is often handled by thrid-party libraries
- on an input sequence this allows to return all errors, not only the first one
- use `Either`
- operations for validation scenarios (depending on the validation libarary the following operations could have different names)
  - transform valid data with map
  - aggregate valid data with validateBoth
  - chain validation rules with FlatMap
  - validate a collection of values with validateEach
- 

## Combining Try and Either
