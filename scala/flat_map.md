# Flat Map

- Similar to [[map_function]]
- General form:

``` scala
F[A].flatMap(A => F[B]) == F[B]
```

- Is a [[transformation]]
- This transformer fucntion creates a collection
- nested collection is directly [[flatten]]ed