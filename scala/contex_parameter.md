# Context Parameter

## Using Context Parameters

- `using` keyword bevor a parameter
- example

  ``` scala
  def f(x: Int)(using a: A, b: B)=...
  ```

- in the examples multiple context parameters are created by the same using clause
- can also be written in a curried way
- using parameter can be passed anonymously without a parameter name
- context parameter can als be defined by a context bound in braces before the parameter list
- example:

``` scala
  def f[a:A](x: Int)= ...
```

## Providing Context Parameters

- scala3 uses `given` definitions
- example of a `given` definition

``` scala
object Ordering:
  given Int: Ordering[Int] with
    def compare(x: Int, y: Int): Int =
      if x < then -1 else if x > y then 1 else 0
```

- given definitions can bi aliased
- as given instances are object they are initialized only once
- given definitions can be anonymous by omitting the name. Compiler will synthesize a name by incorporation of the Types
- the values can also be obtained by summoning the given instances

  ``` scala
    summon[Ordering[Int]]
  ```

  summon is not a special keyword. It is only a predefined method

## context parameter resolution

- for a function that takes a context parameter of type T the compiler will search for an instance that
  - has a type compatible with Type
  - is visible at the point of the method call
- if there are multiple definitions for a suitable context parameter there will be an error because of ambiguity
- givens defined outside of the current scope must be imported
  - by-name
  - by-type this is preferred as it is most informative

  ``` scala
    import scala.math.Ordering.{given Ordering[Int]}
  ```

  - with given selector
- search order for given instances by the compiler:
  - visible instances (inherited or imported)
  - given instances found in associated companion object
- an object that provides given instances is called a [[type_class]]
- given instances can be conditional [[conditional_givens]]

### Priorities between given instances

- actually there can be several different given instance of the same type of one is more specific than another one
- what does more specific mean:
  - depends on the lexical scope

  ``` scala
  given x: Int = 0
  def foo() = 
    given y: Int = 1
    summon[Int] // both given instances would match but y is more specific
  ```

  - depends on the class hierarchy
  
  ``` scala
  class General()
  class Specific() extends General()

  given general : General = General()
  given specific: Specific = Specific()

  summon[General] // both given instance yould match but specific is more specific
  ```

  - depends on the type hierarchy

   ``` scala
   trait A:
     given x: Int = 0
   trait B extends A:
     given y: Int = 1
  
  object C extends B:
    summon[Int] // both given instances would match but given instace in B is more specific

   ```

  - depends on the amount of fixed parts of the types

   ``` scala
   given universal[A]: A = ???
   given int: Int = ???

   summon[Int] // both givens do match but second one is more specific
   ```
