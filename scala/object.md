---
tags: domain-modeling
---
# Object

- referes to the concept of `object` not the runtime object of a class
- like [[class]]es that has exactly one instance (which is automatically instantiated)
- Inizialization is lazy
- Methods in `object` are Similar to static methods in other languages
  - These methods must not have [[side_effect]]s
- Often used for constants or to group static behavior
- To access [[member]]s just call the name of the object and the member with dot notation