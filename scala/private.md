# Private

- A [[visibility]]
- Private [[member]]s are visible from the inside of a [[class]],[[trait]]
or [[object]]
- A [[class]] can access the `private` members of their [[companion_object]]