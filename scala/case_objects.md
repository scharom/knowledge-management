---
tags: domain-modeling
---

# Case Objects

- Useful when a Singelton with some extra functionality is needed
- Similar to [[case_class]]es
- Provide [[pattern_matching]]