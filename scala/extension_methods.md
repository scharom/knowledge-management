# Extension methods

``` scala
    object UserId:
        opaque type UserId = Long
        extension (userId: UserID)
            def value: Long = userID
```

- In this example an extension method is a [[method]]s that are associated with an [[opaque_type]]
- but can be used to extend any [[type]]
- extension method must be visible when called
- compiler will look for extensions only when the called method is missing
- extensions can only add new members, not override existing ones
  - for [[opaque_type]]s the compiler will also look into the scope of the definition of the enclosing structure