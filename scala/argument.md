---
tags: 
---

# Argument

- An arguments are the values that are passed to the [[parameter]]s
- An argument does not have a name
- The evaluated ([[evaluation]]) argument must match the [[type]]s specified by the [[signature]]
