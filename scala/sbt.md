# sbt

- performs incremental compilation: Only compiles changed files and all files that depend on it
- concepts in sbt:
  - settings parametrize the build, are evaluated when sbt loads the project
  - tasks are evaluated each time when they are called
- tasks are parametrized by the values of settings or by results of other tasks
- compilation task will search for source files under `src/main/scala` 
- test task will run compiled binaries of sources under `src/test/scala`
- additional tasks or predefined settings can be added to sbt

## Scoping

- keys can have different values depending on the scoping
- scoping is defined by thee axis:
  - different configurations
  - the particular task
  - sub projects can set their own key values
- if a key has now value in a scope a more general one is used as a fallback (ThisBuild for settings that apply to the entire build)
 
## Dependencies of sbt tasks

![](../attachments/sbt_2021-10-25-11-37-21.png)