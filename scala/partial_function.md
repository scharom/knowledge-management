# Partial Function

- A [[function]] not defined on all of its input domain type
- Before applying a partial function to a value, first check by `isDefined`
