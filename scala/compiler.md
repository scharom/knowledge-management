# Compiler

- is invoked on all source files to turn them into JVM [[bytecode]]
- constructs application class path -> needs dependencies
- generation of resources or assets