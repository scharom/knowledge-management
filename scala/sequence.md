# Sequence

- [[collection]]s that have a well defined order
- is an interface and a subtype of [[iterable]]
- Most of the time this is the insertion order
- Wether one should choose a [[linear_sequence]] or an [[indexed_sequence]] depends on the read and wirte frequencies
- Can be sorted via sortBy