# Side Effect

- Side effects are functions or expressions that modify state outside the local environment
- two types
  - operations that modify the state of the program
  - operations that communicate with the "outside world" (e.g network, files)
- When functions have side effects their order of evaluation does matter
- Downsides of side effects:
  - in a complex code base emphasizing [[modularity]], side effects hinder local reasoning
- The solution are referentially transparent operations -> evaluation output does only depend on the input