---
tags: 
---
# Parameter

- Parameters are located in [[signature]] of a [[method]], [[function]]s or a [[constructor]]
- Parameters can be [[call_by_name]] or [[call_by_value]]