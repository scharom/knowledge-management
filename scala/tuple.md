---
tags: 
---
# Tuple

- A Tuple is closely related to [[collection]]s
- Collection to combine two or more [[type]]s
- accessed with ._1 ._2, or in scala 3 by index
- maximum size is 22 (abitrary convention)
- Can be useful for [[pattern_matching]]
- [[case_class]] should be prefered over tuples