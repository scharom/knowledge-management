---
tags: 
---
# Product ADT

- Is a kind of [[algebraic_data_type]]s
- Grouping modeled concepts together
- Implemented as [[case_class]]es